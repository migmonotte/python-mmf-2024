# Репозиторий курса "Программирование на языке Python".

### Полезные ссылки

* [telegram-чатик курса](https://t.me/python_mmf_2024)
 * [табличка для MR](https://docs.google.com/spreadsheets/d/1Jl0OhcH-LhXsI9fIo58pIfEPMrOuBKUHLq_-udr7OjA/edit?usp=sharing)

### Преподаватели:
Кравчук Артем Витальевич, @akravchuk97 (tg)

Кальмуцкий Кирилл Олегович, @Kirill_Kalmutskiy (tg)

Солдатов Денис Нурхагаевич, @dsoldatov (tg)

### Карта курса

* [week-01](week-01): организационные вопросы, введение в git

* [week-02](week-02): Python, типы и структуры данных, функции

* [week-03](week-03): Декораторы

* [week-04](week-04): Структуры данных, модуль collections

* [week-05](week-05): Разговоры за кодстайл, линтеры, os, argparse

* [week-06](week-06): ООП часть 1

* [week-07](week-07): ООП часть 2, архитектура в Python 

* [week-08](week-08): Исключения