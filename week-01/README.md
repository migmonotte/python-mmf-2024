#### Слайды лекции
 * [тык](https://docs.google.com/presentation/d/17uhn8k2aWEgmYxYzVY2A5UnEZRQt-hVNNWjfPqCYh_o/edit?usp=sharing)
 
#### Слайды семинара (в нем же домашка)
 * [тык](https://docs.google.com/presentation/d/1DQsbaqiykG1aZ_L90rHKloYlql7DwXKKrzJPWgq5wxc/edit?usp=sharing)
 * [табличка для мерж реквестов](https://docs.google.com/spreadsheets/d/1Jl0OhcH-LhXsI9fIo58pIfEPMrOuBKUHLq_-udr7OjA/edit?usp=sharing)

#### Полезные материалы:
 * [скачать git](https://git-scm.com)
 * [визуализатор](https://git-school.github.io/visualizing-git/)
 * [документация git'а](https://git-scm.com/docs)
 * [обучение гиту в игровой форме, очень рекомендую](https://learngitbranching.js.org/?locale=ru_RU)
 