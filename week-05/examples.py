# функции пишем через snake_case
def my_func(a, b):
    return a + b


# def MyFunc(a, b): # так не называем!
#     return a + b

# а классы через CamelCase
class MyClass:
    # а методы класса опять snake_case
    def some_method(self):
        """Useless method"""

# а переменные snake_case
some_variable = 1

# а константы КАПСОМ_С_ПОДЧЕРКУШКАМИ
MY_CONSTANT = 3.1415926

# все запятые и мат. символы обособляем пробелами
# а запятая "приклеивается" к тому, что слева и отделяется пробелом от того, что справа
a, b = 1, 2
z = [1, 2, 3]
some_dict = {'a': 1, 'b': 2}
c = (a - b) ** 2

# пишем условные операторы без скобок, мы за минимализм. if (a == b):  -- это плохо
if a < b:
    c = a + b

if a < b and c < a:
    c = 10


# в функциях запятые у аргументов лепятся по тому же правилу
def some_new_func(a, b):
    """I'm useless"""


# в функциях у кваргов =(равно) не отделяется пробелами!
def some_new_kwarg_func(a, b=1):
    """I'm useless"""


# нужно определиться с одним из видов кавычек и только его использовать в проекте
a = 'asdadasd'
b = "asdasdad"

# импорты объявляем только в начале модуля!
