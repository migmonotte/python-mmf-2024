import argparse
import os


# os.listdir
# os.path.join
# os.path.isdir
# os.path.isfile

def walk(dirpath):
    directories, files = [], []
    for name in os.listdir(dirpath):
        path = os.path.join(dirpath, name)
        if '.git' in name:
            continue
        if os.path.isdir(path):
            directories.append(name)
        else:
            files.append(name)
    print(dirpath, directories, files)
    for name in directories:
        walk(os.path.join(dirpath, name))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path')
    args = parser.parse_args()
    walk(args.path)
