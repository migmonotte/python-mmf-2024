import math


def some_big_operation(some_long_arg, some_another_long_arg_that_longer_that_first):
    return some_long_arg * some_another_long_arg_that_longer_that_first ** some_another_long_arg_that_longer_that_first ** some_another_long_arg_that_longer_that_first


def f(x1, y1, x2, y2):
    result = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    return result


def get_unique_words(text):
    res = []
    for word in text.split():
        if not word in res:
            res.append(word)
    return res


from time import time


def print_function_elapsed_time(func, *args, **kwargs):
    time_start = time()
    func(*args, **kwargs)
    elapsed = time() - time_start
    print(elapsed)


def get_response(name, surname, patrname):
    resp = {
        'name': name,
        'surname': surname,
        'patrname': patrname
    }
    return resp
