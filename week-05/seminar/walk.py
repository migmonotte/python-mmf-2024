import argparse
import os


def walk(path):
    directories = []
    files = []
    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            directories.append(name)
        else:
            files.append(name)
    print(f'root: {path}, directories: {directories}, files: {files}')
    for dirname in directories:
        walk(os.path.join(path, dirname))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_dir', type=str, required=True)
    args = parser.parse_args()
    walk(args.project_dir)



