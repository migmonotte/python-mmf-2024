import os
import sys
from copy import deepcopy
import math


class LogException(Exception):
    def __init__(self, username):
        self._username = username

    def __str__(self):
        return f'Ошибка получена пользователем {self._username}'


def my_open(file_path):
    try:
        with open(file_path) as f:
            return f
    except FileNotFoundError:
        file_path = os.path.abspath(file_path)
        file_dir, file_name = os.path.split(file_path)
        raise FileNotFoundError(
            f'В папке {file_dir} нет файла {file_name}, но есть следующие файлы и папки: {os.listdir(file_dir)}'
        )


class yoda_style:
    def __init__(self):
        self._original_writer = deepcopy(sys.stdout.write)

    def __enter__(self):
        def writer(s):
            self._original_writer(' '.join(reversed(s.split(' '))))

        sys.stdout.write = writer

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.write = self._original_writer


class lock_direcory:
    def __init__(self, dir_path, lock_file):
        self._file_path = os.path.join(dir_path, lock_file)

    def __enter__(self):
        with open(self._file_path, 'w'):
            """do nothing....."""

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self._file_path)

class my_pi_is_better:
    def __init__(self, new_pi):
        self._orig_pi = math.pi
        self._new_pi = new_pi

    def __enter__(self):
        math.pi = self._new_pi

    def __exit__(self, exc_type, exc_val, exc_tb):
        math.pi = self._orig_pi

