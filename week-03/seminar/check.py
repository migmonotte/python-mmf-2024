from functools import wraps
from warnings import warn


def deprecated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        warn('ne ispolzuy menya')
        return func(*args, **kwargs)

    return wrapper


def deprecated_v2(message):
    def deprecated(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            warn(message)
            return func(*args, **kwargs)

        return wrapper

    return deprecated


@deprecated_v2(message='im outdated in version 1.8.5')
def f(x):
    return x


@deprecated_v2(message='im outdated in version 2.0.0')
def g(x):
    return x


def f(*args):
    print(args)


def mock(return_value):
    def decorator(func):
        def wrapper(*args, **kwargs):
            return return_value

        return wrapper

    return decorator


@mock(return_value=':)')
def g(x):
    return x


if __name__ == '__main__':
    print(g(5))
    # print('message')
    # warn('message')
