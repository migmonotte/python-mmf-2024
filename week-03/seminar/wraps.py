from warnings import warn


def wraps(func):
    def decorator(initial_wrapper):
        def wrapper(*args, **kwargs):
            return initial_wrapper(*args, **kwargs)

        wrapper.__name__ = func.__name__
        wrapper.__doc__ = func.__doc__
        wrapper.__module__ = func.__module__
        return wrapper

    return decorator


def deprecated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        warn('ne nado menya trogat')
        return func(*args, **kwargs)

    return wrapper


@deprecated
def f(x):
    "documentation"
    return x


if __name__ == '__main__':
    print(f(1))
    print(f.__name__, f.__doc__)
