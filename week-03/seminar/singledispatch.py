from functools import wraps


def singledispatch(func):
    type2func = {}

    def register(f):
        for arg_type in f.__annotations__.values():
            type2func[arg_type] = f

    @wraps(func)
    def wrapper(*args):
        arg = args[0]
        executor = type2func.get(type(arg), func)

        return executor(*args)

    wrapper.register = register

    return wrapper


@singledispatch
def f(x):
    return 'nothing'


@f.register
def _(x: int):
    print(f'im int: {x}')
    return f'im int: {x}'


@f.register
def _(x: str):
    print(f'im str: {x}')
    return f'im str: {x}'


if __name__ == '__main__':
    f(1)
    print(f.__name__)
