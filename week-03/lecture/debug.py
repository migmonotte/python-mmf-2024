import logging
from functools import wraps

DEBUG = True
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler()
    ]
)
logger = logging.getLogger('debug-logger')


def trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        if DEBUG:
            logger.debug(f'{func.__name__}, args: {args}, kwargs: {kwargs}, result: {res}')

        return res

    return wrapper


@trace
def my_sum(*args):
    """im docstring"""
    res = 0
    for arg in args:
        res += arg
    return res



@trace
def my_max(*args):
    res = float('-inf')
    for arg in args:
        if arg > res:
            res = arg
    return res


@trace
def max_of_sums(*lists):
    sums = []
    for list_ in lists:
        sums.append(my_sum(*list_))
    res = my_max(*sums)

    return res


if __name__ == '__main__':
    args = (
        [-1, -2, -3],
        [-4, -5, -6],
        [0, -100, -10]
    )
    res = max_of_sums(
        *args
    )
