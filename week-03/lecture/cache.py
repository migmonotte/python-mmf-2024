import functools

CACHE = True


def cache(f):
    if not CACHE:
        return f
    _cache = {}

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        key = (args, frozenset(kwargs.items()))
        if key in _cache:
            return _cache[key]
        res = f(*args, **kwargs)
        _cache[key] = res
        return res

    return wrapper


@cache
def fib(n):
    if n <= 1:
        return n
    return fib(n - 1) + fib(n - 2)


if __name__ == '__main__':
    fib(100)
